package advotics;

import java.util.Arrays;

public class NumberUtils {
    
    public void printPrimes(int count) {
        System.out.println(Arrays.toString(getPrimes(count)));
    }

    public int[] getPrimes(int count) {
        int[] founded = new int[count];
        int init = 2;
        int foundedIndex = 0;
        while (foundedIndex < count) {
            if (isPrimeNumber(init)) {
                founded[foundedIndex] = init;
                foundedIndex++;
            }
            init++;
        }
        return founded;
    }

    public boolean isPrimeNumber(int number) {
        int count = 0;
        for (int start = 2; start <= number; start++) {
            if (number % start == 0)
                count++;
            if (count > 1)
                return false;
        }
        return true;
    }

}
