package advotics;

public class StringUtils {

    public boolean isPalindrome(String str) {
        if (str == null || str.length()%2 != 0)
            return false;

        String strLeft = str.substring(0, str.length()/2);
        String strRight = str.substring(str.length()/2);

        for (int index=0; index<strLeft.length(); index++) {
            if (strLeft.charAt(index) != strRight.charAt(strRight.length()-1-index))
                return false;
        }
        return true;
    }
}
