package advotics;

public class Main {
    public static void main(String[] args) {
        NumberUtils numberUtils = new NumberUtils();
        StringUtils stringUtils = new StringUtils();

        // Primes
        numberUtils.printPrimes(5);
        System.out.println(stringUtils.isPalindrome("OKEEKO"));
        System.out.println(stringUtils.isPalindrome("OEKO"));
    }
}
